import React, { Component } from 'react'; 
import axios from "axios";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';


//uri of apis (CRUD)
const url="http://localhost/backend/api/read.php/";
const del="http://localhost/backend/api/delete.php/";
const update="http://localhost/backend/api/update.php/";
const post="http://localhost/backend/api/create.php/";
//uri of api for the select options 
const country="http://localhost/backend/api/select_datas/country.php/";
const status="http://localhost/backend/api/select_datas/status.php/";
const area="http://localhost/backend/api/select_datas/area.php/";
const document="http://localhost/backend/api/select_datas/document.php/";

class App extends Component {

//modals and arrays for create user of cidenet sas :v
     
        
   
state={
  data:[],
  country: [],
  document: [],
  status: [],
  area: [],
   
  modalInsertar: false,
  modalEliminar: false,
  form:{
     
    first_name: '',
    other_name: '',
    first_lastname: '',
    second_lastname: '',
    country: '',
    type_document: '',
    number: '',
    email: '',
    date_ingress: "",
    area: '',
    status: '',
    date_register: ""
  }
}

//functions get 
peticionGet=()=>{
axios.get(url).then(response=>{
  this.setState({data: response.data});
}).catch(error=>{
  console.log(error.message);
})
}

//function post
peticionPost=async()=>{
  delete this.state.form.id_employee;
 await axios.post(post,this.state.form).then(response=>{
     console.log(response);
     console.log(post,this.state.form);
    this.modalInsertar();
    this.peticionGet();
  }).catch(error=>{
    console.log(error.message);
  })
}

//function put
peticionPut=()=>{
  axios.put(update+this.state.form.id_employee, this.state.form).then(response=>{
       console.log(response);
       console.log(update+this.state.form.id_employee, this.state.form);
    this.modalInsertar();
    this.peticionGet();
  })
}

//function delete 
peticionDelete=()=>{
  axios.post(del,{"id_employee": this.state.form.id_employee}).then(response=>{
         console.log(response);
        
    this.setState({modalEliminar: false});
    this.peticionGet();
    this.peticionGet();
  })
}

modalInsertar=()=>{
  this.setState({modalInsertar: !this.state.modalInsertar});
}

//array for update user of cidenet sas
seleccionarEmpresa=(cidenet)=>{
  this.setState({
    tipoModal: 'actualizar',
    form: {
    
      
    id_employee: cidenet.id_employee,
    first_name: cidenet.first_name,
    other_name: cidenet.other_name,
    first_lastname: cidenet.first_lastname,
    second_lastname: cidenet.second_lastname,
    country: cidenet.country,
    type_document: cidenet.type_document,
    number: cidenet.number,
    email: cidenet.email,
    date_ingress: cidenet.date_ingress,
    area: cidenet.area,
    status: cidenet.status,
    date_register: cidenet.date_register
     
    }
  })
}


//save id_employee for delete 
handleChange=async e=>{
e.persist();
await this.setState({
  form:{
    ...this.state.form,
    [e.target.name]: e.target.value
  }
});
console.log(this.state.form);
}

  componentDidMount() {
    this.peticionGet();
  }
  

  render(){
    const {form}=this.state;
    
  return (
    <div className="App">
    <br /><br /><br />
  <button className="btn btn-success" onClick={()=>{this.setState({form: null, tipoModal: 'insertar'}); this.modalInsertar()}}>Agregar Empresa</button>
  <br /><br />
    <table className="table ">
      <thead>
        <tr>
      
          <th>ID</th>
          <th>Nombre completo</th>
          <th>País</th>
          <th>Tipo de documento</th>
          <th>Email</th> 
          <th>Ingreso</th>
          <th>Departamento</th>
          <th>Estado</th>
          <th>Ultimo registro</th>
 
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        {this.state.data.map(cidenet=>{
          return(
            <tr>
              
    
    
          <td>{cidenet.id_employee}</td>
          <td>{cidenet.first_name} {cidenet.other_name} {cidenet.first_lastname} {cidenet.second_lastname}</td>
          <td>{cidenet.country}</td>
          
          <td>{cidenet.type_document}</td>
          <td>{cidenet.email}</td>
          <td>{cidenet.date_ingress}</td>
          <td>{cidenet.area}</td>
          <td>{cidenet.status}</td>
          <td>{cidenet.date_register}</td>
            
          <td>
                <button className="btn btn-primary" onClick={()=>{this.seleccionarEmpresa(cidenet); this.modalInsertar()}}><FontAwesomeIcon icon={faEdit}/></button>
                {"   "}
                <button className="btn btn-danger" onClick={()=>{this.seleccionarEmpresa(cidenet); this.setState({modalEliminar: true})}}><FontAwesomeIcon icon={faTrashAlt}/></button>
                </td>
          </tr>
          )
        })}
      </tbody>
    </table>



    <Modal isOpen={this.state.modalInsertar}>
                <ModalHeader style={{display: 'block'}}>
                  <span style={{float: 'right'}} onClick={()=>this.modalInsertar()}>x</span>
                </ModalHeader>
                <ModalBody>
                  <div className="form-group">
                    
     
                    <label htmlFor="nombre">Primer nombre</label>
                    <input className="form-control" type="text" name="first_name" id="first_name" onChange={this.handleChange} value={form?form.first_name: ''}/>
                    <br />
                    
                    <label htmlFor="nombre">Segundo nombre</label>
                    <input className="form-control" type="text" name="other_name" id="other_name" onChange={this.handleChange} value={form?form.other_name: ''}/>
                    <br />
                    
                    <label htmlFor="nombre">Primer apellido</label>
                    <input className="form-control" type="text" name="first_lastname" id="first_lastname" onChange={this.handleChange} value={form?form.first_lastname: ''}/>
                    <br />
                    
                    <label htmlFor="nombre">Segundo apellido</label>
                    <input className="form-control" type="text" name="second_lastname" id="second_lastname" onChange={this.handleChange} value={form?form.second_lastname: ''}/>
                    <br />
                    
                    <label htmlFor="nombre">Pais</label>
                    <input className="form-control" type="text" name="country" id="country" onChange={this.handleChange} value={form?form.country: ''}/>
                    <br /> 
                    
                    <label htmlFor="nombre">Tipo de documento</label>
                    <input className="form-control" type="text" name="type_document" id="type_document" onChange={this.handleChange} value={form?form.type_document: ''}/>
                    <br />
                     <label htmlFor="nombre">Numero de identificacion</label>
                    <input className="form-control" type="text" name="number" id="number" onChange={this.handleChange} value={form?form.number: ''}/>
                    <br />
             
                     <label htmlFor="nombre">Email</label>
                    <input className="form-control" type="text" name="email" id="email" onChange={this.handleChange} value={form?form.email: ''}/>
                    <br />
                    
                     <label htmlFor="nombre">Departamento</label>
                    <input className="form-control" type="text" name="area" id="area" onChange={this.handleChange} value={form?form.area: ''}/>
                    <br />
                    <label htmlFor="nombre">Estado</label>
                    <input className="form-control" type="text" name="status" id="status" onChange={this.handleChange} value={form?form.status: ''}/>
                    <br />
 
                    <input className="form-control" type="hidden" name="date_ingress" id="date_ingress" onChange={this.handleChange} value={form?form.date_ingress: ''}/>
                  
                     
                  </div>
                </ModalBody>

                <ModalFooter>
                  {this.state.tipoModal=='insertar'?
                    <button className="btn btn-success" onClick={()=>this.peticionPost()}>
                    Insertar
                  </button>: <button className="btn btn-primary" onClick={()=>this.peticionPut()}>
                    Actualizar
                  </button>
  }
                    <button className="btn btn-danger" onClick={()=>this.modalInsertar()}>Cancelar</button>
                </ModalFooter>
          </Modal>


          <Modal isOpen={this.state.modalEliminar}>
            <ModalBody>
               Estás seguro que deseas eliminar de la empresa cidenet a el trabajador con email: {form && form.email}
           
            </ModalBody>
            <ModalFooter>
              <button className="btn btn-danger" onClick={()=>this.peticionDelete()}>Sí</button>
              <button className="btn btn-secundary" onClick={()=>this.setState({modalEliminar: false})}>No</button>
            </ModalFooter>
          </Modal>
  </div>



  );
}
}
export default App;
