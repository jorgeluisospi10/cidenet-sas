<?php
class Employee
{

    // Connection
    private $conn;

    // Table
    private $db_table = "info_employees_view";
    private $db_table1 = "employees_check1";
    private $db_table2 = "employees";

    // Columns
 
    
    public $id_employee;
    public $first_name;
    public $other_name ;
    public $first_lastname;
    public $second_lastname ;
    public $country;
    public $type_document;
    public $number ;
    public $email ;
    public $date_ingress;
    public $area;
    public $status ;
    public $date_register ;  

    // Db connection
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // GET ALL SELECT id_reservations, u.nombre,lugar_inicio, lugar_destino,cupos,d.nombre,d.placa, d.censo,fecha FROM reservations r JOIN drivers d ON r.id_drivers=d.id_dirvers JOIN users u on r.id_users=u.id_users
    public function getEmployees()
    {
      // Table
   
        $sqlQuery = "SELECT * FROM   ". $this->db_table ."";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        return $stmt;
        
    }
    
    

    // CREATE
    public function createEmployee()
    {
    
          

 

        $sqlQuery =  " INSERT INTO `employees` (`id_employee`, `first_name`, `other_name`, `first_lastname`, `second_lastname`, `id_country`, `id_document`, `number`, `email`, `date_ingress`, `id_area`, `id_status`, `date_register`) VALUES (NULL, '". $this->first_name ."', '". $this->other_name ."', '". $this->first_lastname ."', '". $this->second_lastname ."', '". $this->country ."', '". $this->type_document ."', '". $this->number ."', NULL, '". $this->date_ingress ."', '". $this->area ."', '". $this->status ."', '". $this->date_register ."'); 
                    ";
                    
                    //date_ingress = :date_ingress
                    //date_register = :date_register
                    //email = :email,
                    //dejamos afuera los demas campos por que van current_time , ya que no tendran asignacion desde el backend, psdta: mejor en spanish

        $stmt = $this->conn->prepare($sqlQuery);

        // sanitize
 
        $this->first_name = htmlspecialchars(strip_tags($this->first_name));
        $this->other_name = htmlspecialchars(strip_tags($this->other_name));
        $this->first_lastname = htmlspecialchars(strip_tags($this->first_lastname));
        $this->second_lastname = htmlspecialchars(strip_tags($this->second_lastname));
        $this->country = htmlspecialchars(strip_tags($this->country));
        $this->type_document = htmlspecialchars(strip_tags($this->type_document));
        $this->number = htmlspecialchars(strip_tags($this->number));
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->date_ingress = htmlspecialchars(strip_tags($this->date_ingress));
        $this->area = htmlspecialchars(strip_tags($this->area));
        $this->status = htmlspecialchars(strip_tags($this->status));
        $this->date_register = htmlspecialchars(strip_tags($this->date_register));
          
       if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    
    

    // UPDATE
    public function updateEmployee()
    {
     
        $sqlQuery = "
         UPDATE `employees_check1` SET `first_name`='".$this->first_name."',`other_name`='".$this->other_name."',`first_lastname`='".$this->first_lastname."',`second_lastname`='".$this->second_lastname."',`id_country`='".$this->country."',`id_document`='".$this->type_document."',`number`='".$this->number."',`email`='".$this->email."', `id_area`='".$this->area."',`id_status`='".$this->status."' WHERE id_employee='".$this->id_employee."' 
           ";

        $stmt = $this->conn->prepare($sqlQuery);
  // sanitize
        $this->id_employee = htmlspecialchars(strip_tags($this->id_employee));
        $this->first_name = htmlspecialchars(strip_tags($this->first_name));
        $this->other_name = htmlspecialchars(strip_tags($this->other_name));
        $this->first_lastname = htmlspecialchars(strip_tags($this->first_lastname));
        $this->second_lastname = htmlspecialchars(strip_tags($this->second_lastname));
        $this->country = htmlspecialchars(strip_tags($this->country));
        $this->type_document = htmlspecialchars(strip_tags($this->type_document));
        $this->number = htmlspecialchars(strip_tags($this->number));
        $this->email = htmlspecialchars(strip_tags($this->email));
        //$this->date_ingress = htmlspecialchars(strip_tags($this->date_ingress));
        $this->area = htmlspecialchars(strip_tags($this->area));
        $this->status = htmlspecialchars(strip_tags($this->status));
        $this->date_register = htmlspecialchars(strip_tags($this->date_register));
          
       if ($stmt->execute()) {
            return true;
        }
        return false;
    }
      // DELETE
    public function deleteEmployee()
    {
    
          

        $sqlQuery = "DELETE FROM
                      " . $this->db_table1 . 
                      
                      " where id_employee = :id_employee";

        $stmt = $this->conn->prepare($sqlQuery);

        // sanitize
        $this->id_employee = htmlspecialchars(strip_tags($this->id_employee)); 
         

        // bind data
        
    
        $stmt->bindParam(":id_employee", $this->id_employee); 
       if ($stmt->execute()) {
            return true;
        }
        return false;
    }
    
    
    //DEPARTAMENT SELECT 
     public function getArea()
    {
      // Table
   
        $sqlQuery = "SELECT * FROM area";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        return $stmt;
        
    }
     public function getStatus()
    {
      // Table
   
        $sqlQuery = "SELECT * FROM status";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        return $stmt;
        
    }
     public function getCountry()
    {
      // Table
   
        $sqlQuery = "SELECT * FROM country";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        return $stmt;
        
    }
       public function getType()
    {
      // Table
   
        $sqlQuery = "SELECT * FROM document";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        return $stmt;
        
    }


}
