-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 31, 2022 at 05:23 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cidenet`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id_area` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id_area`, `name`) VALUES
(1, 'Administración'),
(2, 'Financiera'),
(3, 'Compras'),
(4, 'Infraestructura'),
(5, 'Operación'),
(6, 'Talento Humano'),
(7, 'Servicios Varios');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id_country` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id_country`, `name`) VALUES
(1, 'Colombia'),
(2, 'Estados Unidos');

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `id_document` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`id_document`, `name`) VALUES
(1, 'Cédula de Ciudadanía'),
(2, 'Cédula de Extranjería'),
(3, 'Pasaporte'),
(4, 'Permiso Especial');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id_employee` int(11) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `other_name` varchar(50) DEFAULT NULL,
  `first_lastname` varchar(20) NOT NULL,
  `second_lastname` varchar(20) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_document` int(11) NOT NULL,
  `number` varchar(20) NOT NULL,
  `email` varchar(300) DEFAULT NULL,
  `date_ingress` date DEFAULT current_timestamp(),
  `id_area` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `date_register` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id_employee`, `first_name`, `other_name`, `first_lastname`, `second_lastname`, `id_country`, `id_document`, `number`, `email`, `date_ingress`, `id_area`, `id_status`, `date_register`) VALUES
(1, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'JLOP1124066936', 'jorge.ospino@cidenet.com.co', '2022-05-27', 1, 1, '2022-05-27 19:35:07'),
(3, 'dd', 'gg', 'xd', 'PASTRANA', 1, 1, 'qwdfs2', 'jj', '2022-05-27', 1, 1, '2022-05-27 21:48:05'),
(4, 'aa', 'ss', '1', 'xd', 1, 1, '1ewr23', 'jlospino', '2022-05-27', 7, 1, '2022-05-27 21:52:33'),
(9, 'qq', 'ww', 'vv', 'aa', 2, 2, '1ewe', '11fg', '2022-05-27', 2, 1, '2022-05-27 22:02:41'),
(10, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, '111', 'werewr', '2022-05-18', 4, 1, '2022-05-27 22:16:01'),
(11, 'JORGE', 'hjhj', 'gfg', 'dff', 2, 1, 'ewwerwer', NULL, '2022-05-27', 1, 1, '2022-05-27 22:24:43'),
(12, 'JORGE', 'jj', 'OSPINO', 'dff', 1, 4, 'fdgfd', 'jorge.ospino@cidenet.com.co', '2022-05-27', 7, 1, '2022-05-27 22:25:58'),
(13, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'dfghr5t', NULL, '2022-05-27', 1, 1, '2022-05-27 22:35:14'),
(14, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'fghr56', NULL, '2022-05-27', 1, 1, '2022-05-27 22:37:33'),
(15, 'JORGE', 'LUIS', 'OSPINO', 'PAST', 1, 1, 'gfhgf555', NULL, '2022-05-27', 1, 1, '2022-05-27 22:38:51'),
(16, 'sdsdf', 'wqe', 'qwqw', 'qwqw', 1, 1, 'eew', NULL, '2022-05-27', 5, 1, '2022-05-27 22:44:40'),
(17, 'a', 's', 's', 'a', 1, 1, 'a', NULL, '2022-05-27', 1, 1, '2022-05-27 22:47:18'),
(18, 'a', 's', 's', 'a', 1, 1, 'dsfdf3', NULL, '2022-05-27', 1, 1, '2022-05-27 22:48:38'),
(144, 'a', 'a', 'GUTIERRES', 's', 1, 1, 'vvv44', NULL, '2022-05-27', 4, 1, '2022-05-27 22:52:51'),
(145, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'ERTWE43', NULL, '2022-05-27', 1, 1, '2022-05-27 23:00:34'),
(146, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'SDFS434', NULL, '2022-05-27', 1, 1, '2022-05-27 23:02:08'),
(211, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'gffgfgr54', NULL, '2022-05-27', 1, 1, '2022-05-27 23:10:49'),
(345, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'fgd4', NULL, '2022-05-27', 1, 1, '2022-05-27 23:24:35'),
(1455, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'JLOP1124066936', NULL, '2022-05-27', 1, 1, '2022-05-27 23:06:13'),
(1456, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'dfghr5t', NULL, '2022-05-27', 1, 1, '2022-05-27 23:08:51'),
(1457, 'marii', 'andrea', 'sarmiento', 'perez', 1, 1, 'fghf45', NULL, '2022-05-27', 5, 1, '2022-05-27 23:12:06'),
(1458, 'jaun', 'leon', 'jose', 'aa', 2, 2, 'sdf2323', NULL, '2022-05-27', 6, 1, '2022-05-27 23:13:22'),
(1459, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'dfg3', NULL, '2022-05-27', 6, 1, '2022-05-27 23:16:17'),
(1460, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'ret', NULL, '2022-05-27', 4, 1, '2022-05-27 23:22:15'),
(1461, 'JORGE', 'jj', 'OSPINO', 'gdfg', 1, 1, 'rty545', NULL, '2022-05-27', 3, 1, '2022-05-27 23:30:47'),
(1462, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'dsfsdf3', ' ', '2022-05-27', 1, 1, '2022-05-27 23:46:32'),
(1463, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'dfgdf', NULL, '2022-05-27', 1, 1, '2022-05-27 23:49:30'),
(1464, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'fgh5', 'jl', '2022-05-27', 1, 1, '2022-05-27 23:52:13'),
(1465, 'xxx', 'asd', 'peres', 'PASTRANA', 1, 1, 'dfdf45', NULL, '2022-05-27', 1, 1, '2022-05-27 23:59:03'),
(1468, 'manuel', NULL, 'de la rosa', 'perez', 1, 1, '11234443', NULL, '2022-05-28', 1, 1, '2022-05-28 07:59:35'),
(1469, 'pablo', NULL, 'de jesus', 'ortega', 2, 2, 'JLOP1124066936df', NULL, '2022-05-28', 3, 1, '2022-05-28 08:05:08'),
(1470, 'gd', 'dfdf', 'gdfg', 'dfgdfs', 1, 1, 'dffg', NULL, '2022-05-28', 1, 1, '2022-05-28 08:26:47'),
(1471, 'sdfdsf', 'sdsd', 'ortega mendes', 'fsdds', 2, 2, '4345', 'fdfdf', '2022-05-28', 3, 1, '2022-05-28 08:27:13'),
(1472, 'sofi', NULL, 'del carmen', 'ss', 1, 1, '22343dge', NULL, '2022-05-28', 1, 1, '2022-05-28 08:29:00'),
(1473, 'dfgdfgdfg', 'dfsdf', 'tauro sol aguirre', 'sdsdf', 2, 2, 'dsfsd434rdf', NULL, '2022-05-28', 7, 1, '2022-05-28 08:38:14'),
(1474, 'dhgh', 'fh', 'ghgf', 'fggh', 1, 1, 'fgdh', NULL, '2022-05-28', 1, 1, '2022-05-28 09:31:08'),
(1475, 'xxx', 'xxx', 'xxx', 'xxx', 1, 1, 'xxx', NULL, '2022-05-28', 1, 1, '2022-05-28 09:31:08'),
(1476, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'fggh23', NULL, '2022-05-28', 1, 1, '2022-05-28 17:03:08'),
(1477, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'fggh23', NULL, '2022-05-28', 1, 1, '2022-05-28 17:06:26'),
(1478, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'fggh23', NULL, '2022-05-28', 1, 1, '2022-05-28 17:08:50'),
(1479, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'fggh23', NULL, '2022-05-28', 1, 1, '2022-05-28 17:19:59'),
(1480, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'fggh23', NULL, '2022-05-28', 1, 1, '2022-05-28 17:20:36'),
(1481, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'fggh23', NULL, '2022-05-28', 1, 1, '2022-05-28 17:20:38'),
(1482, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 1, 1, 'fggh23', NULL, '2022-05-28', 1, 1, '2022-05-28 17:20:39'),
(1483, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 2, 2, 'fggh23', NULL, '2022-05-28', 1, 1, '2022-05-28 17:21:12'),
(1484, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 2, 2, 'fggh23', NULL, '2022-05-28', 1, 1, '2022-05-28 17:21:13'),
(1485, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 2, 2, 'fggh23', NULL, '2022-05-28', 1, 1, '2022-05-28 17:21:13'),
(1486, 'JORGE', 'LUIS', 'OSPINO', 'PASTRANA', 2, 2, 'fggh23', NULL, '2022-05-28', 1, 1, '2022-05-28 17:22:27'),
(1487, 'ivan', 'jose', 'duque', 'falso', 1, 1, '123443', NULL, '2022-05-29', 1, 1, '2022-05-29 21:10:01'),
(1488, 'petro', 'jorge', 'presidente', 'dfg', 1, 1, '1213423', NULL, '2022-05-29', 1, 1, '2022-05-29 21:15:39'),
(1489, 'dfdf', 'gdff', 'dff', 'dgdfg', 1, 3, '443', NULL, '2022-05-29', 1, 1, '2022-05-29 21:20:28'),
(1490, 'petro', 'presidente', 'presidente', 'presidente', 1, 1, '121211', NULL, '2022-05-29', 1, 1, '2022-05-29 21:22:15'),
(1491, 'Petro ', 'jose', 'presidente', 'ok', 1, 1, '121212', NULL, '2022-05-29', 1, 1, '2022-05-29 21:24:34'),
(1492, 'Petro', 'andres', 'presidente', 'ah', 1, 1, '324234dsds', NULL, '2022-05-29', 1, 1, '2022-05-29 21:26:34'),
(1493, 'ghgh', 'ghjgh', 'ghgh', 'erter', 1, 1, '112332', NULL, '2022-05-29', 1, 1, '2022-05-29 22:28:35'),
(1494, 'q', 'qw', 'qqw', 'qww', 1, 1, '1', NULL, '2022-05-30', 1, 1, '2022-05-30 00:18:29'),
(1495, '2', '234', 'er', 'sd', 2, 1, '2', NULL, '2022-05-30', 1, 1, '2022-05-30 00:22:00'),
(1496, '234', '234234', '23', '324', 1, 1, '1', NULL, '2022-05-30', 1, 1, '2022-05-30 01:37:01'),
(1497, '1', '2', '3', '4', 2, 1, '1', NULL, '2022-05-30', 1, 1, '2022-05-30 03:22:03'),
(1498, ' jorge ', ' luis', 'de la cruz', 'salcedo', 1, 1, '12', NULL, '2022-05-30', 1, 1, '2022-05-30 03:22:48'),
(1499, '1', '2', '1', '1', 2, 1, '1', NULL, '2022-05-30', 1, 1, '2022-05-30 13:51:11'),
(1500, '1', '1', '1', '1', 1, 1, '1', NULL, '2022-05-30', 1, 1, '2022-05-30 13:52:15'),
(1501, '1', '1', '1', '1', 2, 3, '4', NULL, '2022-05-30', 2, 2, '2022-05-30 13:54:00'),
(1502, '2', '1', '1', '1', 2, 1, '1', NULL, '2022-05-30', 1, 1, '2022-05-30 13:57:38'),
(1503, '1', '1', '2', '3', 1, 1, '6', NULL, '2022-05-30', 2, 1, '2022-05-30 17:43:24'),
(1504, '111', 'jorge', '111', 'ospino', 1, 1, '1', NULL, '2022-05-30', 1, 1, '2022-05-30 17:45:55'),
(1505, 'jorge', 'andres', 'ospino', 'aaa', 1, 1, '1', NULL, '2022-05-30', 1, 1, '2022-05-30 17:47:02'),
(1506, '1', '1', '2', '1', 1, 1, '1', NULL, '2022-05-31', 1, 1, '2022-05-31 02:23:03'),
(1507, '11', '1', 'LOCA', '1', 1, 1, '0002890392', NULL, '2022-05-30', 2, 1, '2022-05-30 21:53:50');

--
-- Triggers `employees`
--
DELIMITER $$
CREATE TRIGGER `create_email` BEFORE INSERT ON `employees` FOR EACH ROW BEGIN
#dominios of email  REPLACE(@email_col, ' ', '')
SET @us ="@cidenet.com.us";
SET @col ="@cidenet.com.co";
SET @c= 113;
SET @sum = (SELECT COUNT(*) from employees_check1 WHERE employees_check1.first_name=NEW.first_name and employees_check1.first_lastname=NEW.first_lastname);
 
SET @email_col= CONCAT(NEW.first_name,'.',NEW.first_lastname,@col);
SET @email_us= CONCAT(NEW.first_name,'.',NEW.first_lastname,@us);
SET @email_col_id= CONCAT(NEW.first_name,'.',NEW.first_lastname,'.',@sum,@col);
SET @email_us_id= CONCAT(NEW.first_name,'.',NEW.first_lastname,'.',@sum,@us);
SET @type_document= NEW.id_document;
#select id_country for select the dominio :v
SET @country = (SELECT employees.id_country from employees WHERE employees.id_employee=NEW.id_employee);
 
 #verification email
SET @quantity_col = (SELECT COUNT(employees_check1.id_employee) as total from employees_check1 WHERE employees_check1.email=@email_col  );
SET @quantity_us = (SELECT COUNT(employees_check1.id_employee) as total from employees_check1 WHERE employees_check1.email=@email_us  );
SET @quantity_col_id = (SELECT COUNT(employees_check1.id_employee) as total from employees_check1 WHERE employees_check1.email=@email_col_id  );
SET @quantity_us_id = (SELECT COUNT(employees_check1.id_employee) as total from employees_check1 WHERE employees_check1.email=@email_us_id );
#craete email and delete spaces :v espacios en blancos
  IF (NEW.id_country = 1 and @quantity_col = 0 )
              THEN
              INSERT INTO employees_check1 ( employees_check1.id_employee,employees_check1.first_name,employees_check1.other_name,employees_check1.first_lastname,employees_check1.second_lastname,employees_check1.id_country,employees_check1.id_document,employees_check1.number,employees_check1.email,employees_check1.date_ingress,employees_check1.id_area,employees_check1.id_status,employees_check1.date_register) VALUES ( NEW.id_employee, NEW.first_name,NEW.other_name, NEW.first_lastname,NEW.second_lastname, NEW.id_country,NEW.id_document, NEW.number,REPLACE(@email_col, ' ', ''), NEW.date_ingress, NEW.id_area, NEW.id_status, NEW.date_register);
     END IF;
     
  IF (NEW.id_country = 2 and @quantity_us = 0 )
              THEN
              INSERT INTO employees_check1 ( employees_check1.id_employee,employees_check1.first_name,employees_check1.other_name,employees_check1.first_lastname,employees_check1.second_lastname,employees_check1.id_country,employees_check1.id_document,employees_check1.number,employees_check1.email,employees_check1.date_ingress,employees_check1.id_area,employees_check1.id_status,employees_check1.date_register) VALUES ( NEW.id_employee, NEW.first_name,NEW.other_name, NEW.first_lastname,NEW.second_lastname, NEW.id_country,NEW.id_document, NEW.number,REPLACE(@email_us, ' ', '') , NEW.date_ingress, NEW.id_area, NEW.id_status, NEW.date_register);
     END IF;
     IF (NEW.id_country = 1 and @quantity_col >= 1 )
              THEN
              INSERT INTO employees_check1 ( employees_check1.id_employee,employees_check1.first_name,employees_check1.other_name,employees_check1.first_lastname,employees_check1.second_lastname,employees_check1.id_country,employees_check1.id_document,employees_check1.number,employees_check1.email,employees_check1.date_ingress,employees_check1.id_area,employees_check1.id_status,employees_check1.date_register) VALUES ( NEW.id_employee, NEW.first_name,NEW.other_name, NEW.first_lastname,NEW.second_lastname, NEW.id_country,NEW.id_document, NEW.number,REPLACE(@email_col_id, ' ', ''), NEW.date_ingress, NEW.id_area, NEW.id_status, NEW.date_register);
     END IF;
     
  IF (NEW.id_country = 2 and @quantity_us >= 1 )
              THEN
              INSERT INTO employees_check1 ( employees_check1.id_employee,employees_check1.first_name,employees_check1.other_name,employees_check1.first_lastname,employees_check1.second_lastname,employees_check1.id_country,employees_check1.id_document,employees_check1.number,employees_check1.email,employees_check1.date_ingress,employees_check1.id_area,employees_check1.id_status,employees_check1.date_register) VALUES ( NEW.id_employee, NEW.first_name,NEW.other_name, NEW.first_lastname,NEW.second_lastname, NEW.id_country,NEW.id_document, NEW.number,REPLACE(@email_us_id, ' ', ''), NEW.date_ingress, NEW.id_area, NEW.id_status, NEW.date_register);
     END IF;
      



END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `validate_document` BEFORE INSERT ON `employees` FOR EACH ROW BEGIN

SET @val = (SELECT count(employees.id_employee) as total from employees WHERE employees.id_document=NEW.id_document and employees.number=NEW.number);

IF @val>=1 THEN 
    signal sqlstate '45000';
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `validate_email` BEFORE INSERT ON `employees` FOR EACH ROW BEGIN

SET @val = (SELECT count(employees.email) as total from employees WHERE employees.email=NEW.email);

IF @val>=1 THEN 
    signal sqlstate '45000';
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `employees_check1`
--

CREATE TABLE `employees_check1` (
  `id_employee` int(11) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `other_name` varchar(50) DEFAULT NULL,
  `first_lastname` varchar(20) NOT NULL,
  `second_lastname` varchar(20) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_document` int(11) NOT NULL,
  `number` varchar(20) NOT NULL,
  `email` varchar(300) NOT NULL,
  `date_ingress` date DEFAULT current_timestamp(),
  `id_area` int(11) NOT NULL,
  `id_status` int(11) NOT NULL,
  `date_register` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees_check1`
--

INSERT INTO `employees_check1` (`id_employee`, `first_name`, `other_name`, `first_lastname`, `second_lastname`, `id_country`, `id_document`, `number`, `email`, `date_ingress`, `id_area`, `id_status`, `date_register`) VALUES
(14, 'jorge', 'jorge', 'ospino', 'ospino', 1, 1, '1', 'jorge.ospino@cidenet.com.co', '2022-05-30', 1, 1, '2022-05-30 17:45:55'),
(16, '1', '1', '2', '1', 1, 1, '1', '1.2@cidenet.com.co', '2022-05-31', 1, 1, '2022-05-31 02:23:03'),
(18, '11', '1', 'LOCA', '1', 1, 1, '0002890392', '11.LOCA@cidenet.com.co', '2022-05-30', 2, 1, '2022-05-30 21:53:50');

--
-- Triggers `employees_check1`
--
DELIMITER $$
CREATE TRIGGER `update_mail` BEFORE UPDATE ON `employees_check1` FOR EACH ROW BEGIN
#dominios of email
SET @us ="@cidenet.com.us";
SET @col ="@cidenet.com.co";
SET @c= 113;
SET @sum = (SELECT COUNT(*) from employees_check1 WHERE employees_check1.first_name=NEW.first_name and employees_check1.first_lastname=NEW.first_lastname);

SET @email_col= CONCAT(NEW.first_name,'.',NEW.first_lastname,@col);
SET @email_us= CONCAT(NEW.first_name,'.',NEW.first_lastname,@us);
SET @email_col_id= CONCAT(NEW.first_name,'.',NEW.first_lastname,'.',@sum,@col);
SET @email_us_id= CONCAT(NEW.first_name,'.',NEW.first_lastname,'.',@sum,@us);
SET @type_document= NEW.id_document;
#select id_country for select the dominio :v
SET @country = (SELECT employees.id_country from employees WHERE employees.id_employee=NEW.id_employee);
 
 #verification email
SET @quantity_col = (SELECT COUNT(employees_check1.id_employee) as total from employees_check1 WHERE employees_check1.email=@email_col  );
SET @quantity_us = (SELECT COUNT(employees_check1.id_employee) as total from employees_check1 WHERE employees_check1.email=@email_us  );
SET @quantity_col_id = (SELECT COUNT(employees_check1.id_employee) as total from employees_check1 WHERE employees_check1.email=@email_col_id  );
SET @quantity_us_id = (SELECT COUNT(employees_check1.id_employee) as total from employees_check1 WHERE employees_check1.email=@email_us_id );

  IF (NEW.id_country = 1 and @quantity_col = 0 )
              THEN
                SET NEW.email = REPLACE(@email_col, ' ', '');
              
     END IF;
     
  IF (NEW.id_country = 2 and @quantity_us = 0 )
              THEN
                              SET NEW.email = REPLACE(@email_us, ' ', '');
     END IF;
     IF (NEW.id_country = 1 and @quantity_col >= 1 )
              THEN
                            SET NEW.email =REPLACE(@email_col_id, ' ', '');
     END IF;
     
  IF (NEW.id_country = 2 and @quantity_us >= 1 )
              THEN
    SET NEW.email=REPLACE(@email_us_id, ' ', '');

     END IF;
      



END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `info_employees_view`
-- (See below for the actual view)
--
CREATE TABLE `info_employees_view` (
`id_employee` int(11)
,`first_name` varchar(20)
,`other_name` varchar(50)
,`first_lastname` varchar(20)
,`second_lastname` varchar(20)
,`country` varchar(30)
,`type_document` varchar(30)
,`number` varchar(20)
,`email` varchar(300)
,`date_ingress` date
,`area` varchar(20)
,`status` varchar(15)
,`date_register` datetime
);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id_status` int(11) NOT NULL,
  `name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id_status`, `name`) VALUES
(1, 'activo'),
(2, 'inactivo');

-- --------------------------------------------------------

--
-- Structure for view `info_employees_view`
--
DROP TABLE IF EXISTS `info_employees_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `info_employees_view`  AS SELECT `e`.`id_employee` AS `id_employee`, `e`.`first_name` AS `first_name`, `e`.`other_name` AS `other_name`, `e`.`first_lastname` AS `first_lastname`, `e`.`second_lastname` AS `second_lastname`, `c`.`name` AS `country`, `d`.`name` AS `type_document`, `e`.`number` AS `number`, `e`.`email` AS `email`, `e`.`date_ingress` AS `date_ingress`, `a`.`name` AS `area`, `s`.`name` AS `status`, `e`.`date_register` AS `date_register` FROM ((((`employees_check1` `e` join `status` `s`) join `document` `d`) join `country` `c`) join `area` `a`) WHERE `e`.`id_country` = `c`.`id_country` AND `e`.`id_document` = `d`.`id_document` AND `e`.`id_area` = `a`.`id_area` AND `e`.`id_status` = `s`.`id_status` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id_area`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id_country`);

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id_document`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id_employee`),
  ADD KEY `id_country` (`id_country`),
  ADD KEY `id_document` (`id_document`),
  ADD KEY `id_area` (`id_area`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `employees_check1`
--
ALTER TABLE `employees_check1`
  ADD PRIMARY KEY (`id_employee`),
  ADD KEY `id_country` (`id_country`),
  ADD KEY `id_document` (`id_document`),
  ADD KEY `id_area` (`id_area`),
  ADD KEY `id_status` (`id_status`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id_status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id_area` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id_country` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `id_document` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id_employee` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1508;

--
-- AUTO_INCREMENT for table `employees_check1`
--
ALTER TABLE `employees_check1`
  MODIFY `id_employee` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`id_area`) REFERENCES `area` (`id_area`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `employees_ibfk_2` FOREIGN KEY (`id_country`) REFERENCES `country` (`id_country`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `employees_ibfk_3` FOREIGN KEY (`id_status`) REFERENCES `status` (`id_status`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `employees_ibfk_4` FOREIGN KEY (`id_document`) REFERENCES `document` (`id_document`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
