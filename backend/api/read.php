<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    
    include_once '../config/database.php';
    include_once '../class/employees.php';

    $database = new Database();
    $db = $database->getConnection();

    $items = new Employee($db);

    $stmt = $items->getEmployees();
    $itemCount = $stmt->rowCount();


 
//data of view employees
    if($itemCount > 0){
        
        $employeeArr = array();
        
 

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
            
 

                "id_employee" => $id_employee,
                "first_name" => $first_name,
                "other_name" => $other_name,
                "first_lastname" => $first_lastname,
                "second_lastname" => $second_lastname,
                "country" => $country,
                "type_document" => $type_document,
                "number" => $number,
                "email" => $email,
                "date_ingress" => $date_ingress,
                "area" => $area,
                "status" => $status,
                "date_register" => $date_register 
            );

            array_push($employeeArr, $e);
        }
        echo json_encode($employeeArr);
    }

    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found." )
        );
    }
