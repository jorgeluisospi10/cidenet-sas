<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once '../config/database.php';
    include_once '../class/employees.php';

    $database = new Database();
    $db = $database->getConnection();

    $item = new Employee($db);

    $data = json_decode(file_get_contents("php://input"));
  
      
                
    $item->first_name = $data->first_name;
    $item->other_name = $data->other_name;
    $item->first_lastname = $data->first_lastname;
    $item->second_lastname = $data->second_lastname;
    $item->country = $data->country;
    $item->type_document = $data->type_document;
    $item->number = $data->number;
    $item->email = $data->email;
    $item->date_ingress = date('Y-m-d'); 
    $item->area = $data->area;
    $item->status = $data->status;
    $item->date_register = date('Y-m-d H:i:s'); 
    if($item->createEmployee()){
        echo 'created successfully.';
    } else{
        echo 'could not be created.';
    }
    
    
